#'@export
find_missing <- function(..., request, by = "SUBJECT_LAST_NAME") {

  x <- list(...)

  by_names <- names(by)
  for(i in seq(length(by))) {
    if(is.null(by_names[i])) {
      names(by)[i] <- by[[i]]
    }
  }

  for(i in seq(length(x))) {
    x[[i]] <- x[[i]][, by]
  }

  x <- do.call(rbind, x)

  missing <- request %>%
    anti_join(x, by = by)

  return(missing)

}