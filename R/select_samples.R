#'@export

select_samples = function(query,dir,protocol = NULL,available = TRUE,collection_group = NULL){

  if(!is.null(protocol)){
    query = query%>%dplyr::filter(PROTOCOL_NO==protocol)
  }
  if(!is.null(collection_group)){
    query = query%>%dplyr::filter(COLLECTION_GROUP==collection_group)
  }
  if(available){
    query = query%>%dplyr::filter(SPECIMEN_STATUS=="Available")
  }

  x = nrow(query)

  if(missing(dir)){
    dir = mmgeDistributions:::get_pullsheet_config()$directory
  }

  if(file.exists(file.path(dir,"R","samples"))){

    if(grepl("csv",list.files(file.path(dir,"R","samples")))){
      samplelist = read.csv(file.path(dir,"R","samples",list.files(file.path(dir,"R","samples"))))
    }

    if(grepl("xlsx",list.files(file.path(dir,"R","samples")))){
      samplelist = readxl::read_excel(file.path(dir,"R","samples",list.files(file.path(dir,"R","samples"))))
    }

    variables = names(samplelist)
    if(any(grepl("bar|code|sample",variables,ignore.case = T))){
      variables = variables[c(grep("bar|code|sample",variables,ignore.case = T,invert = T),grep("bar|code|sample",variables,ignore.case = T))]
    }
    p=0
    for (j in length(variables):1){
      samplelistchar = as.character(dplyr::pull(samplelist,variables[j]))
      samplelist[,j]=samplelistchar
      if(any(as.vector(na.omit(samplelistchar))%in%as.vector(na.omit(query$SPECIMEN_BAR_CODE)))&mean(nchar(samplelistchar))>4&p<1){
        query = dplyr::semi_join(query,samplelist,by=c("SPECIMEN_BAR_CODE"=variables[j]))
        p=p+1
      }
    }
    if(any(grepl("CASE_NO|case",variables,ignore.case = T))){
      variables = variables[c(grep("CASE_NO|case",variables,ignore.case = T,invert = T),grep("CASE_NO|case",variables,ignore.case = T))]
    }
    if(x==nrow(query)){
      for (j in length(variables):1){
        samplelistchar = as.character(dplyr::pull(samplelist,variables[j]))
        samplelist[,j]=samplelistchar
        if(any(as.vector(na.omit(samplelistchar))%in%as.vector(na.omit(query$CASE_NO)))&mean(nchar(samplelistchar))>4&p<1){
          query = dplyr::semi_join(query,samplelist,by=c("CASE_NO"=variables[j]))
          p=p+1
        }
      }
    }
    if(any(grepl("subject|patient|mrn",variables,ignore.case = T))){
      variables = variables[c(grep("subject|patient|mrn",variables,ignore.case = T,invert = T),grep("subject|patient|mrn",variables,ignore.case = T))]
    }
    if(x==nrow(query)){
      for (j in length(variables):1){
        samplelistchar = as.character(dplyr::pull(samplelist,variables[j]))
        samplelist[,j]=samplelistchar
        if(any(as.vector(na.omit(samplelistchar))%in%as.vector(na.omit(query$PATIENT_ID)))&mean(nchar(samplelistchar))>4&p<1){
          query = dplyr::semi_join(query,samplelist,by=c("PATIENT_ID"=variables[j]))
          p=p+1
        }
      }
    }
    if(x==nrow(query)){
      for (j in length(variables):1){
        samplelistchar = as.character(dplyr::pull(samplelist,variables[j]))
        samplelist[,j]=samplelistchar
        if(any(as.vector(na.omit(samplelistchar))%in%as.vector(na.omit(query$ALTERNATE_MRN)))&mean(nchar(samplelistchar))>4&p<1){
          query = dplyr::semi_join(query,samplelist,by=c("ALTERNATE_MRN"=variables[j]))
          p=p+1
        }
      }
    }
    if(x==nrow(query)){
      for (j in length(variables):1){
        samplelistchar = as.character(dplyr::pull(samplelist,variables[j]))
        samplelist[,j]=samplelistchar
        if(any(as.vector(na.omit(samplelistchar))%in%as.vector(na.omit(query$SUBJECT_LAST_NAME)))&mean(nchar(samplelistchar))>4&p<1){
          query = dplyr::semi_join(query,samplelist,by=c("SUBJECT_LAST_NAME"=variables[j]))
          p=p+1
        }
      }
    }
  }
  if(x==nrow(query)){
    message("No match available")
  }
  return(query)
}