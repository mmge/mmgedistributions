summarysheet = function(...,pull,aliquot,extract,combine,dir="."){
  config = get_pullsheet_config(dir)
  if(is.null(aliquot)){
    both = pull
  }
  if(is.null(pull)){
    both = aliquot
  }
  if(!is.null(aliquot)&!is.null(pull)){
    both = dplyr::full_join(pull,aliquot)
  }
  if(!is.null(combine)){
    combine = combine%>%
      dplyr::group_by(CASE_NO)%>%
      dplyr::filter(row_number()==1)
    both = both%>%
      dplyr::full_join(combine)
  }
  extra_sheets <- list(...)
  if(is.null(extract)){
    if(is.null(aliquot)){
      summary1 = data.frame(V1=1,V2=1:5)
      summary1$V1=c("# Samples","# Subjects","# to be subaliquoted (except Whole Blood)",
                    "# of Whole Blood to be subaliquoted","# hemoglobin testing")
      summary1$V2=c(nrow(both),
                    eval(length(unique(both[["PATIENT_ID"]]))),
                    0,
                    0,
                    eval(ifelse(is.null(config$hemoglobin_level)==F,
                                eval(ifelse(config$hemoglobin_level=="case",nrow(both[which(is.na(both$HEMOGLOBIN_ASSAY)==T),]),0)),0)))
    }
    else{
      summary1 = data.frame(V1=1,V2=1:5)
      summary1$V1=c("# Samples","# Subjects","# to be subaliquoted (except Whole Blood)",
                    "# of Whole Blood to be subaliquoted","# hemoglobin testing")
      summary1$V2=c(nrow(both),
                    eval(length(unique(both[["PATIENT_ID"]]))),
                    nrow(aliquot[which(aliquot[["SPECIMEN_TYPE"]]!="Whole Blood"),]),
                    nrow(aliquot[which(aliquot[["SPECIMEN_TYPE"]]=="Whole Blood"),]),
                    eval(ifelse(is.null(config$hemoglobin_level)==F,
                                eval(ifelse(config$hemoglobin_level=="case",nrow(both[which(is.na(both$HEMOGLOBIN_ASSAY)==T),]),0)),0)))
    }
  }
  else{
    both = dplyr::bind_rows(both,extract)
    if(is.null(aliquot)){
      summary1 = data.frame(V1=1,V2=1:10)
      summary1$V1=c("# Samples","# Subjects","# to be subaliquoted (except Whole Blood)",
                    "# of Whole Blood to be subaliquoted","# hemoglobin testing",
                    "# of DNA extractions from Whole Blood","# of DNA extractions from Saliva",
                    "# of DNA extractions from Tissue","# of RNA (Total) extractions","# of RNA (micro) extractions")
      summary1$V2=c(nrow(both),
                    eval(length(unique(both[["PATIENT_ID"]]))),
                    0,
                    0,
                    eval(ifelse(is.null(config$hemoglobin_level)==F,
                                eval(ifelse(config$hemoglobin_level=="case",nrow(both[which(is.na(both$HEMOGLOBIN_ASSAY)==T),]),0)),0)),
                    nrow(extract[which(extract[["SPECIMEN_TYPE"]]%in%c("Whole Blood","Plasma","Buffy Coat")),]),
                    nrow(extract[which(extract[["SPECIMEN_TYPE"]]=="Saliva"),]),
                    nrow(extract[which(extract[["SPECIMEN_TYPE"]]=="Tissue"),]),
                    nrow(extract[which(extract[["COLLECTION_GROUP"]]=="RNA"),]),
                    nrow(extract[which(extract[["COLLECTION_GROUP"]]=="RNA"),]))
    }
    else{
      summary1 = data.frame(V1=1,V2=1:10)
      summary1$V1=c("# Samples","# Subjects","# to be subaliquoted (except Whole Blood)",
                    "# of Whole Blood to be subaliquoted","# hemoglobin testing",
                    "# of DNA extractions from Whole Blood","# of DNA extractions from Saliva",
                    "# of DNA extractions from Tissue","# of RNA (Total) extractions","# of RNA (micro) extractions")
      summary1$V2=c(nrow(both),
                    eval(length(unique(both[["PATIENT_ID"]]))),
                    nrow(aliquot[which(aliquot[["SPECIMEN_TYPE"]]!="Whole Blood"),]),
                    nrow(aliquot[which(aliquot[["SPECIMEN_TYPE"]]=="Whole Blood"),]),
                    eval(ifelse(is.null(config$hemoglobin_level)==F,
                                eval(ifelse(config$hemoglobin_level=="case",nrow(both[which(is.na(both$HEMOGLOBIN_ASSAY)==T),]),0)),0)),
                    nrow(extract[which(extract[["SPECIMEN_TYPE"]]%in%c("Whole Blood","Plasma","Buffy Coat")),]),
                    nrow(extract[which(extract[["SPECIMEN_TYPE"]]=="Saliva"),]),
                    nrow(extract[which(extract[["SPECIMEN_TYPE"]]=="Tissue"),]),
                    nrow(extract[which(extract[["COLLECTION_GROUP"]]=="RNA"),]),
                    nrow(extract[which(extract[["COLLECTION_GROUP"]]=="RNA"),]))
    }
  }
  colnames(summary1)=c("Pullsheet Summary","#")
  return(summary1)
}