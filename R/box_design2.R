#'@export
design_box2 <- function(..., balance_vars, box_size, subject_id,
                       maximize_fullness = FALSE, dir = ".", verbose = FALSE) {

  sheets <- list(...)

  for(i in seq(length(sheets))) {
    sheets[[i]] <- dplyr::ungroup(sheets[[i]])
  }

  config <- get_pullsheet_config(dir)

  if(missing(subject_id)) {
    subject_id <- config$subject_id$blinded
  }

  if(missing(box_size)) {
    box_size <- config$box_size
  }

  if(missing(balance_vars)) {
    balance_vars = config$balance_vars
  }

  box_design <- do.call(rbind, sheets) %>%
    dplyr::distinct_(.dots = c(subject_id, "VISIT"), .keep_all = TRUE) %>%
    dplyr::select_(.dots = c(subject_id, "VISIT", balance_vars)) %>%
    dplyr::group_by_(.dots = c(subject_id, balance_vars)) %>%
    dplyr::summarise(VISITS = n()) %>%
    dplyr::arrange_(.dots = balance_vars) %>%
    dplyr::mutate(FINAL_BOX_NO = 0) %>%
    dplyr::ungroup()

  if(maximize_fullness) {
    box_design <- box_design %>%
      dplyr::arrange(VISITS == 1)
  }

  box_count <- ceiling(sum(box_design$VISITS) / box_size)
  current_box <- 1
  i <- 1

  if(maximize_fullness) {
    box_breakdown <- box_design %>%
      dplyr::group_by_(.dots = balance_vars) %>%
      dplyr::summarize(n = sum(VISITS) / sum(box_design$VISITS))
    for(box_no in seq(box_count - 1)) {
      for(y in seq(nrow(box_breakdown))) {
        sample_count <- round(box_size * box_breakdown$n[y])
        z <- box_design %>%
          dplyr::semi_join(box_breakdown[y, ], by = balance_vars) %>%
          dplyr::filter(FINAL_BOX_NO == 0) %>%
          dplyr::ungroup() %>%
          dplyr::sample_n(size = min(sample_count, nrow(.))) %>%
          dplyr::mutate(FINAL_BOX_NO = box_no)
        box_design <- box_design %>%
          dplyr::anti_join(z, by = config$subject_ids$blinded) %>%
          rbind(z)
      }
    }
    box_design$FINAL_BOX_NO[box_design$FINAL_BOX_NO == 0] <- box_count
    box_design <- consolidate_boxes(box_design, box_size = box_size)

  } else {

    increment <- function(current_box, box_count) {
      current_box <- current_box + 1
      if(current_box > box_count) {
        current_box <- 1
      }
      return(current_box)
    }

    while(i <= nrow(box_design)) {
      current_box_count <- sum(box_design$VISITS[box_design$FINAL_BOX_NO == current_box])
      if(box_design$VISITS[i] <= box_size - current_box_count) {
        box_design$FINAL_BOX_NO[i] <- current_box
        i <- i + 1
      }
      current_box <- increment(current_box, box_count)
    }

  }

  return(box_design)

}

calc_fullness_score <- function(box_design, box_size) {

  x <- box_design %>%
    dplyr::group_by(FINAL_BOX_NO) %>%
    dplyr::summarize(COUNT = sum(VISITS))

  x$REMAINING = box_size - x$COUNT

  return(sum(x$REMAINING) - x$REMAINING[x$FINAL_BOX_NO == max(x$FINAL_BOX_NO)])

}

consolidate_boxes <- function(box_design, tolerance = 2, max_iters = 50, box_size, verbose = FALSE) {

  tolerance = 2
  max_iters = 50

  cnt = 1
  best_bd <- box_design
  while(cnt <= max_iters & calc_fullness_score(best_bd, box_size) > tolerance) {

    bd <- box_design

    for(i in which(bd$FINAL_BOX_NO == max(bd$FINAL_BOX_NO))) {

      x <- bd %>%
        dplyr::group_by(FINAL_BOX_NO) %>%
        dplyr::summarize(n = sum(VISITS)) %>%
        dplyr::mutate(REMAINING = box_size - n) %>%
        dplyr::filter(REMAINING >= box_design$VISITS[i]) %>%
        dplyr::filter(REMAINING == min(REMAINING)) %>%
        dplyr::filter(FINAL_BOX_NO != max(box_design$FINAL_BOX_NO))
      if(nrow(x) > 0) {
        if(nrow(x) == 1) {
          b <- x$FINAL_BOX_NO
        } else {
          b = sample(x$FINAL_BOX_NO, size = 1)
        }
        if(verbose) message("Adding sample to Box #", b)
        bd$FINAL_BOX_NO[i] = b
      }

    }

    if(calc_fullness_score(bd, box_size) < calc_fullness_score(best_bd, box_size)) {
      best_bd <- bd
    }

    cnt <- cnt + 1
  }

  if(calc_fullness_score(best_bd, box_size) > tolerance) {
    message("Could not meet fullness criteria")
  }

  return(best_bd)

}


